<?php

/**
 * @file
 * Installation file for UUID Map module.
 */

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Implements hook_schema().
 */
function uuid_map_schema() {
  $schema['uuid_map'] = [
    'description' => 'Mapps uuids to entity types.',
    'fields' => [
      'uuid' => [
        'description' => 'The uuid for the given entity.',
        'type' => 'varchar_ascii',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'entity_type' => [
        'description' => 'The entity_type for the entity_id this record affects.',
        'type' => 'varchar_ascii',
        'length' => EntityTypeInterface::ID_MAX_LENGTH,
        'not null' => TRUE,
        'default' => '',
      ],
      'entity_id' => [
        'description' => 'The entity_id this record affects.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => ['uuid', 'entity_type', 'entity_id'],
    'indexes' => [
      'entity' => ['entity_type', 'entity_id'],
    ],
  ];

  return $schema;
}
